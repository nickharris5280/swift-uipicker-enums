//
//  AppDelegate.swift
//  EnumPoweredPickerView
//
//  Created by Nick Harris on 12/5/15.
//  Copyright © 2015 Nick Harris. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }
}

