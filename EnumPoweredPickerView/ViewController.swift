//
//  ViewController.swift
//  EnumPoweredPickerView
//
//  Created by Nick Harris on 12/5/15.
//  Copyright © 2015 Nick Harris. All rights reserved.
//

import UIKit

enum PickerOptions: Int, CustomStringConvertible {
    case Daily = 0
    case Weekly = 1
    case Monthly = 2
    case Bimonthly = 3
    case Quarterly = 4
    case Yearly = 5
    case Biannual = 6
    case Biennial = 7
    
    static var count: Int { return PickerOptions.Biennial.rawValue + 1 }
    
    var description: String {
        switch self {
        case .Daily: return "Every Day"
        case .Weekly   : return "Weekly"
        case .Monthly  : return "Every Month"
        case .Bimonthly : return "Twice a Month"
        case .Quarterly : return "Every Three Months"
        case .Yearly : return "Every Year"
        case .Biannual : return "Twice a Year"
        case .Biennial : return "Every Other Year"
        }
    }
    
    var value: Int {
        switch self {
        case .Daily: return 1
        case .Weekly   : return 7
        case .Monthly  : return 30
        case .Bimonthly : return 60
        case .Quarterly : return 90
        case .Yearly : return 360
        case .Biannual : return 180
        case .Biennial : return 720
        }
    }
}

class ViewController: UIViewController, UIPickerViewDataSource {

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return PickerOptions.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard let choreRecurrenceOption = PickerOptions(rawValue: row) else {
            fatalError("Unknown PickerOption")
        }
        
        return choreRecurrenceOption.description
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let choreRecurrenceOption = PickerOptions(rawValue: row) else {
            fatalError("Unknown PickerOption")
        }
        
        print("PickerOption.description: \(choreRecurrenceOption.description) | PickerOption.value: \(choreRecurrenceOption.value)")
    }
    
}

